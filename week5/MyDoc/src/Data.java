abstract  class Data{
    abstract public void DisplayValue();
}
class Integer extends Data{
    int value;
    Integer(){
        value = 10324;
    }

    @Override
    public void DisplayValue() {
        System.out.println (value);
    }
}
class Byte extends Data{
    byte value;
    Byte(){
        value=123;
    }
    @Override
    public  void DisplayValue(){
        System.out.println (value);}
    }

    abstract class Factory{
        /**
         *
         */
    public Data CreateDataObject;

        public abstract Data CreateDtaObject();

        public Data CreateDataObject() {
        }
    }
    class IntFactory extends Factory{
        @Override
        public Data CreateDtaObject() {
            return null;
        }
    }
    class ByteFactory extends Factory{
    public Data CreateDataObject(){
        return new Byte();
    }

        @Override
        public Data CreateDtaObject() {
            return null;
        }
    }
