public class MyDoc {
    static Document a;
    public static  void main (String[] args){
        a=new Document(new ByteFactory());
        a.DisplayData();
    }

    class Document{
        Data pa;
        Document(Factory pf){
            pa = pf.CreateDataObject();
        }
        public void DisplayData(){
            pa.DisplayValue();
        }
    }
}
