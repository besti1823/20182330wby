public class Capacity {
    public static void main(String[] args){
   //实例化一个对象，赋值
        StringBuffer s1 = new StringBuffer("jnj");
   //试试看capacity和length有什么区别
   //     s1.ensureCapacity(16);
        System.out.println("this inital capacity:" +s1.capacity());
        System.out.println("this length:"+ s1.length());

        //实例化一个对象,append方法
        StringBuffer s2 = new StringBuffer();
        s2.ensureCapacity(34);
        s2.append(" Hello Wbjhhjhkjhjhkjhkjhkjhjhkjhkjhjkhjkhjkhjkorld!!! ");
        System.out.println("this inital capacity:" +s2.capacity());
        System.out.println("this length:"+ s2.length()+"\n");

        StringBuffer sb1 = new StringBuffer();
        for (int num = 0; num < 5; num++) {
            sb1.append("12345678");

            System.out.println(" this capacity: " + sb1.capacity());
            System.out.println(" this length: " + sb1.length());
            System.out.println("---------------------------------");
        }
        StringBuffer sb = new StringBuffer("cccccccccccc");
        System.out.println("sb:" + sb);
        System.out.println("sb.capacity():" + sb.capacity());
        System.out.println("sb.length():" + sb.length());

        StringBuffer sBuffer =new StringBuffer();
        sBuffer.append("cccccccccccc");//12个c
        System.out.println("SBuffer's capacity"+sBuffer.capacity());

    }

}
