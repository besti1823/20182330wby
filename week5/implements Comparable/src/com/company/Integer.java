package com.company;
import java.util.Scanner;
public class Integer
{
    public static void  Integer() {
        Scanner sc = new Scanner(System.in);
        float a;
        float b;
        float c;
        System.out.println("Please input a:");
        a = sc.nextFloat();
        System.out.println("Please input b:");
        b = sc.nextFloat();
        while (b == 0) {
            System.out.println("Please retry!");
            b = sc.nextFloat();
        }
        System.out.println("Please choose your algorithm：（+ — * / %）");
        String op = sc.next();
        switch (op) {
            case "+":
                System.out.println(a + "+" + b + "=" + (a + b));
                break;
            case "-":
                System.out.println(a + "-" + b + "=" + (a - b));
                break;
            case "*":
                System.out.println(a + "*" + b + "=" + (a * b));
                break;
            case "/":
                System.out.println(a + "/" + b + "=" + (a / b));
                break;
            case "%":
                System.out.println(a + "%" + b + "=" + (a - (int) (a / b) * b));
                break;
            default:
                System.out.println("Error in Identification"
                        + " Character.");
        }
    }
}
