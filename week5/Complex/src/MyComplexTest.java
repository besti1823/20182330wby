import junit.framework.TestCase;
import org.junit.Test;

public class MyComplexTest extends TestCase {
    MyComplex a = new MyComplex(4,5);
    MyComplex b = new MyComplex(3,7);
    MyComplex c = new MyComplex(2.7,9.5);
    MyComplex d = new MyComplex(8,2.1);
    MyComplex e = new MyComplex(8,15);
    @Test
    public void testgetRealPart() throws Exception {
        assertEquals(4.0,a.getRealPart());
        assertEquals(2.7,c.getRealPart());
    }
    @Test
    public void testgetImagePart()throws Exception{
        assertEquals(7.0,b.getImagePart());
        assertEquals(2.1,d.getImagePart());
    }
    @Test
    public void testComplexAdd()throws Exception{
        assertEquals(7.0,a.ComplexAdd(b).realpart);
        assertEquals(11.6,c.ComplexAdd(d).imagepart);
    }
    @Test
    public void testComplexSub()throws Exception{
        assertEquals(1.0,a.ComplexSub(b).realpart);
        assertEquals(7.4,c.ComplexSub(d).imagepart);
    }
    @Test
    public void testComplexMul()throws Exception{
        assertEquals(-23.0,a.ComplexMul(b).realpart);
        assertEquals(81.67,c.ComplexMul(d).imagepart);
    }
    @Test
    public void testComplexchufa()throws Exception{
        assertEquals(12.9,e.ComplexDiv(b).realpart);
        assertEquals(3.0,e.ComplexDiv(a).imagepart);
    }
}