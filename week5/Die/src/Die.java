public class Die
{
    private final int MAX = 6;//maxinum face value
    private int faceValue;//current  value showing on the die
    public Die()
    {
        faceValue = 1;
    }
    public int roll()
    {
        faceValue = (int)(Math.random()*MAX)+1;
        return faceValue;
    }
    public void setFaceValue(int value)
    {
        faceValue = value;
    }
    public int getFaceValue()
    {
        return faceValue;
    }
    public String toString()
    {
        String result = Integer.toString(faceValue);
        return result;
    }
    public int getFaceDown()
    {
        return (MAX+1)-faceValue;
    }
    //Returns true if this Die equals Die,otherwise returns false.
    public boolean equals(Die die)
    {
        return(this.faceValue == die.faceValue);
    }
}