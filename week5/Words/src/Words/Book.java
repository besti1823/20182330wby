package Words;

import Words.*;

public class Book {
    protected int pages = 1500;

    public void setPages (int numPages) {
        this.pages = numPages;
    }

    public int getPages() {
        return pages;
    }
}
