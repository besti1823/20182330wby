package PP8_8;

public class System {
    private String name ="";
    private int time;
    private String miaoshu="";
    private String suoshuyuanxi="";

    public System(String n, int t, String m, String s){
        name = n;
        time = t;
        miaoshu = m;
        suoshuyuanxi = s;
    }

    public String getName{
        return name;
    }

    public String getMiaoshu{
        return miaoshu;
    }

    public int getTime{
        return time;
    }

    public int getSuoshuyuanxi{
        return suoshuyuanxi;
    }
}
