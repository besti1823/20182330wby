package Interface;

public  class PeopleTest{
    public static void main(String[] args){
        Interface.People student = new Interface.People() {
            public void play() {
                System.out.println("Basketball!");
            }

            public void study() {
                System.out.println("Highmath");
            }
        };
        student.play();
        student.study();
    }
}