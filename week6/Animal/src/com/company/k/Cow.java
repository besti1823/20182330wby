public class Cow extends Animal
{


    public Cow(String name, String id) {
        super(name, id);
    }

    @Override
    public void feed() {
        System.out.println("grass!");
    }
}
