public class Rabbit extends Animal
{


    public Rabbit(String name, String id) {
        super(name, id);
    }

    @Override
    public void feed() {
        System.out.println("carrot!");
    }
}
