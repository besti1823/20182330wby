public abstract class Animal {

   private String name;
   private String  id;

   public Animal (String name,String id){
   this.name = name;
   this.id = id;
   }

   public Animal(String name) {
      this.name = name;
   }

   public String getName() {
      return name;
   }

   public String getId() {
      return id;
   }

   @Override
   public String toString() {
      return "Animal{" +
              "name='" + name + '\'' +
              ", id='" + id + '\'' +
              '}';
   }

   public abstract void feed();
}


