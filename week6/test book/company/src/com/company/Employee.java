package com.company;

public class Employee extends StaffMember {
    protected String socialSecuritNumber;
    protected double payRate;

    public Employee (String eName,String eAddress,String ePhone,String socSecNumber,double rate){
        super(eName, eAddress, ePhone);

        socialSecuritNumber = socSecNumber;
        payRate = rate;
    }

    public String toString(){
        String result = super.toString();
        result +="\nSocial Security Number: " + socialSecuritNumber;
        return result;
    }
    public double pay(){
        return payRate;
    }

}
