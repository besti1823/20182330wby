public interface Speaker {
    public void Listen();
    public void Speak();
}