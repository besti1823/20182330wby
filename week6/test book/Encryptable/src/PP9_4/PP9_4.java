public class SpeakerTest {
    public static void main(String[] args) {
        Speaker guest;
        guest = new Philosopher();

        System.out.println((Philosopher)guest+" :");
        guest.Listen();
        guest.Speak();
        ((Philosopher) guest).Read();
        ((Philosopher)guest).Write();
        ((Philosopher)guest).Think();

        System.out.println();

        guest = new Dog();
        System.out.println((Dog)guest+" :");
        guest.Listen();
        guest.Speak();
    }
}