package com.company;

public interface Encryptable{
    public void encrypt();
    public String decrypt();
    //继承类文件
}
