package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class TaskTest {
    public static void main(String[] args) {
        test[] list=new test[4];
        int[] a=new int[4];
        List<test> tasklist=new ArrayList<>();

        test task=new test("");
        list[0]=new test("Java1");
        list[1]=new test("Java2");
        list[2]=new test("Java3");
        list[3]=new test("Java4");

        for(int i=0;i<4;i++){
            tasklist.add(list[i]);
        }

        Scanner in=new Scanner(System.in);
        System.out.println("请输入一组优先级：(数字越大优先级越高）");
        for(int i=0;i<4;i++){
            a[i]=in.nextInt();//接受优先级
        }

        test.set(list,a);
        for(test c:list){
            System.out.println(c.getName()+",优先级:"+c.getPriority());
        }
        //尝试列表排序
        //   System.out.println("排序后：");
        // Collections.sort(tasklist);
        //for(Task c:tasklist){
        //    System.out.println(c.getName()+",优先级:"+c.getPriority());
        // }

        Arrays.sort(list);
        for(test c:list){
            System.out.println(c.getName()+",优先级:"+c.getPriority());
        }

    }
}