package com.company;


public class test implements Priority,Comparable<test> {
    private int priority;
    private String name;
    private test[] t;
    private int[] n;

    public test(String name) {
        this.name = name;
    }

    @Override
    public void setPriority(int n) {
        this.priority=n;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public void set(test[] t, int[] n) {

    }

    public void set(test[] t,int[] n){
        this.t = t;//建立Task【】，输入要做的事
        this.n = n;
        for(int i=0;i<t.length;i++){
            t[i].setPriority(n[i]);
        }
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int compareTo(test c) {//比较优先级
        if(this.priority>c.priority)
            return 1;
        else if(this.priority<c.priority)
            return -1;
        else
            return 0;
    }

}