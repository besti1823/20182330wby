package com.company;

import java.util.Random;
//继承上一个
public class Secret implements Encryptable {
    private String mes;
    private boolean encrypted;
    private int shift;
    private Random generator;

    public  Secret (String msg){
        mes = msg;
        encrypted = false;
        generator = new Random();
        shift = generator.nextInt(10) + 5;
    }

    public void encrypt() {
        if(!encrypted){
            String masked = "";
            for (int index=0;index<mes.length();index++)
                masked = masked + (char) (mes.charAt(index) + shift);
                mes = masked;
                encrypted = true;
        }
    }

    public String decrypt(){
        if(encrypted){
            String unmasked = "";
            for(int index = 0;index<mes.length();index++)
                unmasked = unmasked + (char)(mes.charAt(index)-shift);
            mes = unmasked;
            encrypted = false;
        }
        return mes;
    }

    public boolean isEncrypted(){
        return encrypted;
    }

    public String toString(){
        return mes;
    }
}
