package com.company;

public class Staff {

    private StaffMember[] staffList;
    public Staff()
    {
        staffList = new StaffMember[6];
        staffList[0] = new Executive ("Tony","123 Main Line","555-0469","123-45-6789",2423.07);

        staffList[1] = new Employee ("Paulie","456 Off Line","555-0101","987-654-321",1246.15);
        staffList[2] = new Employee ("Vito","789 Off Rocker","555-0000","957-45-6789",1231.43);

        staffList[3] = new Hourly("Michael","678 Fifth Ave","555-0101","123-45-6789",2423.07);

        staffList[4] = new Volunteer ("Adrianna","987 Main Line","555-0101","123-45-6789",2423.07);
        staffList[5] = new Volunteer ("Benny","123 Main Line","555-0101","123-45-6789",2423.07);

        ((Executive)staffList[0]).awardBonus(500.00);
        ((Hourly)staffList[3]).addHours (40);
    }

    public void payday()
    {
        double amount;
        for(int count=0;count<staffList.length;count++)
        {
            System.out.println(staffList[count]);
            amount = staffList[count].pay();
            if(amount == 0.0)
                System.out.println("Thanks!");
            else
                System.out.println("Paid: "+amount);
            System.out.println("-------------------------");
        }
    }
}
