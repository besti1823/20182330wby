package com.company;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class Complex {
    public static String complex(String duchu){
        String r1;
        String i1;
        String r2;
        String i2;
        float R1;
        float I1;
        float R2;
        float I2;
        float R;
        float I;
        String answer=null;
        ArrayList list = new ArrayList();
        int x=0;
        StringTokenizer st = new StringTokenizer(duchu);
        while(st.hasMoreTokens()){
            //info[x]=st.nextToken();
            list.add(st.nextToken());
        }
        //       a = info[0];
        r1 = (String) list.get(3);
        i1= (String) list.get(0);
        r2= (String) list.get(7);
        i2= (String) list.get(4);
        R1=Float.parseFloat(r1);
        R2=Float.parseFloat(r2);
        I1=Float.parseFloat(i1);
        I2=Float.parseFloat(i2);

        I=I1+I2;
        R=R1+R2;
        answer=I+"i"+R+"";
        return answer;
    }
}
