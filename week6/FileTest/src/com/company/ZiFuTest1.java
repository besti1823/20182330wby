package com.company;
import java.io.*;

/**
 * Created by besti on 2018/5/6.
 */
public class ZiFuTest1 {
    public static void main(String[] args) throws IOException {

        //文件创建（文件类实例化）
        File file = new File("～\\用户\\wby\\IdeaProjects\\FileTest", "Hey.txt");
        if (!file.exists()) {
            file.createNewFile();
        }

        //Writer写入文件
        Writer writer2 = new FileWriter(file);
        writer2.write("Hello, I/O Operataion!这是利用Writer写入文件的内容");
        writer2.flush();
        writer2.append("Hello,World");
        writer2.flush();

        //Reader读出
        Reader reader2 = new FileReader(file);
        System.out.println("下面是用Reader读出的数据：");
        while(reader2.ready()){
            System.out.print((char) reader2.read()+ "  ");
    }
}
}

