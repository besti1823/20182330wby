package com.company;
import java.io.*;

/**
 * Created by besti on 2018/5/6.
 */
public class ZiJieTest2 {
    public static void main(String[] args) throws IOException {
        //文件创建（文件类实例化）
        File file = new File("～\\用户\\wby\\IdeaProjects\\FileTest", "Hey.txt");
        if (!file.exists()) {
            file.createNewFile();
        }

        //BufferedOutputstream字节写入
        OutputStream outputStream2 = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(outputStream2);
        String content2 = "利用BufferedOutputStream写入文件的缓冲区内容";
        bufferedOutputStream2.write(content2.getBytes(),0,content2.getBytes().length);
        bufferedOutputStream2.flush();
        bufferedOutputStream2.close();

        //BufferedInputstream字节读出
        byte[] buffer = new byte[1024];
        String content = "";
        int flag = 0;
        InputStream inputStream2 = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);

        while ((flag =bufferedInputStream.read(buffer))!=-1){
            content += new String(buffer,0,flag);
        }

        System.out.println(content);
        bufferedInputStream.close();
        System.out.println("文件读结束：BufferedInputStream直接读并输出！");


    }
}