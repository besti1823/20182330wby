package com.company;
import java.io.*;

/**
 * Created by besti on 2018/5/6.
 */
public class ZiFuTest2 {
    public static void main(String[] args) throws IOException {
        //文件创建（文件类实例化）
        File file = new File("～\\用户\\wby\\IdeaProjects\\FileTest", "Hey.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        //BufferedWriter写入
        Writer writer2 = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(writer2);
        String content3 = "使用bufferedWriter写入";
        bufferedWriter.write(content3,0,content3.length());
        bufferedWriter.flush();
        bufferedWriter.close();

        //BufferedReader读出
        String content = "";
        Reader reader2 = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader2);
        System.out.println("\n下面是用BufferedReader读出的数据：");
        while ((content =bufferedReader.readLine())!= null){
            System.out.println(content);
        }
    }
}
