package com.company;
import java.io.*;
import java.util.Scanner;

/**
 * Created by besti on 2018/5/6.
 */
public class ComplexStream {
    public static void main(String[] args) throws IOException {
        double real1,real2,image1,image2,R,I;
        //文件创建（文件类实例化）
        File file = new File("～\\用户\\wby\\IdeaProjects\\FileTest", "Hey.txt");
        if (!file.exists()) {
            file.createNewFile();
        }

        //1.Outputstream写入两个复数
        OutputStream outputStream1 = new FileOutputStream(file);
        System.out.println("Please input two complex: (eg: 2 i + 1   3 i + 1 ) ");
        Scanner sc=new Scanner(System.in);
        String str=null;//
        str=sc.nextLine();

        byte[] shuru=null;
        shuru=str.getBytes();//将字符转化成byte类型
        outputStream1.write(shuru);//将byte型字符写入文件
        outputStream1.flush();//可有可无，不执行任何操作


        //2.Inputstream读出两个复数
        InputStream inputStream1= new FileInputStream(file);
        byte[] str1 = inputStream1.readAllBytes();
        String str2= new String(str1);
        String duchu=Complex.complex(str2);
        inputStream1.close();

        //3.BufferOutput写入复数之和
        OutputStream outputStream2 = new FileOutputStream(file);
        byte[] answer=null;
        answer = duchu.getBytes();//将读出的字符串转化为byte型
        outputStream1.write(answer);//将answer写入文件
        outputStream1.flush();
        System.out.println("文件读结束！");
    }
}

