package com.company;
import java.io.*;

/**
 * Created by besti on 2018/5/6.
 */
public class ZiJieTest1{
    public static void main(String[] args) throws IOException {

        //文件创建（文件类实例化）
        File file = new File("～/IdeaProjects/FileTest", "hey.txt");
        if (!file.exists()) {
            file.createNewFile();
        }

        //OutputStream写入
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] hello = {'H', 'e', 'l', 'l', 'o', ',', 'W', 'o', 'r', 'l', 'd', '!'};
        outputStream1.write(hello);
        outputStream1.flush();//可有可无，不执行任何操作！！！

        //InputStream读出
        InputStream inputStream1 = new FileInputStream(file);
        while (inputStream1.available() > 0) {
            System.out.print((char) inputStream1.read() + "  ");
        }
        inputStream1.close();
        System.out.println("\n文件读写结束：OutputStream、InputStream先写后读");
    }
}