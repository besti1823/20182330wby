package com.company;
//继承
public class StringTooLongException extends RuntimeException {
    public StringTooLongException(String message){
        super(message);
    }
}