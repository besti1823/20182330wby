package com.company;

import com.company.StringTooLongException;

import java.util.*;
//输入字符
class StringExceptionTest {
    public static void main(String[] args) {
        StringTooLongException S1 = new StringTooLongException("too long ");
        Scanner scan  = new Scanner(System.in);
        String A = "";

        System.out.print("Please input the String ");
        A = scan.nextLine();
        try {
            while (!A.equals("DONE")) {
                if (A.length() > 20) {
                    throw S1;
                }
                System.out.println(A);
                System.out.print("Please input the string:");
                A = scan.nextLine();
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
    }
}