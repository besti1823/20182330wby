
import javafoundations.*;

public class Maze
{

  private final int TRIED = 3;

  private final int PATH = 7;

  private int [][] grid = { {1,1,1,0,1,1,0,0,0,1,1,1,1},
                            {1,0,0,1,1,0,1,1,1,1,0,0,1},
                            {1,1,1,1,1,0,1,0,1,0,1,0,0},
                            {0,0,0,0,1,1,1,0,1,0,1,1,1},
                            {1,1,1,0,1,1,1,0,1,0,1,1,1},
                            {1,0,1,0,0,0,0,1,1,1,0,0,1},
                            {1,0,1,1,1,1,1,1,0,1,1,1,1},
                            {1,0,0,0,0,0,0,0,0,0,0,0,0},
                            {1,1,1,1,1,1,1,1,1,1,1,1,1} };

  private StackADT<Position> push_new_pos(int x, int y, 
                                          StackADT<Position> stack)
  {
    Position npos = new Position();
    npos.setx(x);
    npos.sety(y);
    if (valid(npos.getx(),npos.gety()))
      stack.push(npos);

    return stack;
  }
  

  public boolean traverse ()
  {
    boolean done = false;
    Position pos = new Position();
    Object dispose;
    StackADT<Position> stack = new ArrayStack<Position>();
    stack.push(pos);
    
    while (!(done))
    {
      pos = stack.pop();
      grid[pos.getx()][pos.gety()] = TRIED;  // this cell has been tried
      if (pos.getx() == grid.length-1 && pos.gety() == grid[0].length-1)
        done = true;  // the maze is solved
      else
      {
        stack = push_new_pos(pos.getx(),pos.gety() - 1, stack);
        stack = push_new_pos(pos.getx(),pos.gety() + 1, stack); 
        stack = push_new_pos(pos.getx() - 1,pos.gety(), stack); 
        stack = push_new_pos(pos.getx() + 1,pos.gety(), stack);
      }
    }
    
    return done;
  }
  

  private boolean valid (int row, int column)
  {
    boolean result = false;
    

    if (row >= 0 && row < grid.length &&
        column >= 0 && column < grid[row].length)
      

      if (grid[row][column] == 1)
        result = true;
    
    return result;
  }
  

  public String toString ()
  {
    String result = "\n";
    
    for (int row=0; row < grid.length; row++)
    {
      for (int column=0; column < grid[row].length; column++)
        result += grid[row][column] + "";
      
      result += "\n";
    }
    
    return result;
  }
}
