
import javafoundations.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class NewGraphStackGUI extends JPanel
{
   private JButton pushButton;
   private JButton popButton;
   private JTextArea currentStack;
   private JLabel inputLabel;
   private JTextField currentInput;
   private JTextArea currentAction;
   private JPanel panel;
   private LinkedStack<String> myStack = new LinkedStack<String>();

  public NewGraphStackGUI()
  {
    pushButton = new JButton("Push");
    pushButton.addActionListener(new PushListener());
    popButton = new JButton("Pop");
    popButton.addActionListener(new PopListener());
    
    inputLabel = new JLabel("Add to stack: ");
    currentInput = new JTextField(18);
    currentInput.setEditable(true);
    currentStack = new JTextArea(6, 20);
    currentStack.setMargin(new Insets(5,5,5,5));
    currentStack.setEditable(false);
    currentAction = new JTextArea(3,20);
    currentAction.setMargin(new Insets(5,5,5,5));
    currentAction.setEditable(false);
    currentAction.setText("");
    
    panel = new JPanel();
    panel.add(inputLabel);
    panel.add(currentInput);
    panel.add(pushButton);
    panel.add(popButton);
    
    setLayout(new BorderLayout());
    add(new JScrollPane(currentStack), BorderLayout.NORTH);
    add(panel, BorderLayout.CENTER);
    add(new JScrollPane(currentAction), BorderLayout.SOUTH);
    setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
  }

  public void display()
  {
    JFrame frame = new JFrame("Stack Graphics Demo");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(this);
    
    frame.pack();
    frame.setVisible(true);
  }

  private class PushListener implements ActionListener
  {

    public void actionPerformed (ActionEvent event)
    {
      String toPush = currentInput.getText();
      if (toPush.length() !=0)
      {
        myStack.push(toPush);
        currentAction.append("\"" + toPush+"\"" + " was pushed onto " +
                              "the stack.\n");
        currentStack.setText(myStack.toString());
        currentStack.setCaretPosition( 0 ); // forces scroll up
        currentInput.setText(null); // clears input field
      }
      else
        currentAction.append("Cannot push empty data.\n");
    }
  }


  private class PopListener implements ActionListener
  {

    public void actionPerformed (ActionEvent event)
    {
      if (! myStack.isEmpty())
      {
        String popped = myStack.pop();
        currentAction.append("\"" + popped +"\"" + " was popped off " +
                             "the stack.\n");    
        currentStack.setText(myStack.toString());
        currentStack.setCaretPosition( 0 );
        currentInput.setText(null);
      }
      else
        currentAction.append("Cannot pop from an empty stack.\n");
    }
  }
}
