package com.company;

import com.company.LinkStack;

public class Stack {
    public static void main(String[] args) {
        LinkStack arrayStack = new LinkStack();

        arrayStack.push(20182330);
        arrayStack.push(20182329);
        arrayStack.push(20182331);

        System.out.println("栈是否为空：" + arrayStack.isEmpty());
        System.out.println("栈顶为： " + arrayStack.peek());
        System.out.println("栈：" + arrayStack.toString());

        arrayStack.pop();//弹栈
        System.out.println("栈大小：" + arrayStack.size());
        System.out.println("栈：" + arrayStack.toString());


    }
}