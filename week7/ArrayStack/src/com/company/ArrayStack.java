package com.company;

import java.util.Arrays;
import java.util.Stack;


public class ArrayStack<T> implements StackADT<T>{
    private final int DEFAULT_CAPACITY = 100;

    private int top;
    private T[] stack;

    // 使用默认容量，创建一个空栈
    public ArrayStack()
    {
        top = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    // 使用指定容量，创建一个空栈
    public ArrayStack (int initialCapacity)
    {
        top = 0;
        stack = (T[]) (new Object[initialCapacity]);
    }
    //push
    public void push (T element)
    {
        if (size() == stack.length)
        {
            expandCapacity();
        }
        stack[top] = element;
        top++;
    }
    //容量
    public int size() {
        return top;
    }
    //扩容
    private void expandCapacity()
    {
        stack = Arrays.copyOf(stack, stack.length * 2);
    }

    public T pop()
    {
        if (isEmpty())
            throw new EmptyCollectionException("Stack");
        top--;
        T result = stack[top];
        stack[top] = null;

        return  result;
    }
    //判断是否为空
    public boolean isEmpty() {
        if(stack[0] == null)
        {
            return true;
        }
        return false;
    }

    //查看栈顶元素
    public T peek() {
        if (isEmpty())
            throw new EmptyCollectionException("Stack");

        return stack[top - 1];
    }

    public String toString()
    {
        String a = "";
        for (int x = 0;x < top; x++)
        {
            a += stack[x] + " ";

        }
        return a;
    }
}