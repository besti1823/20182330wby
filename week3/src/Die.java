/*================================================================
*   Copyright (C) 2019 20182330-wby All rights reserved.
*   
*   文件名称：Die.java
*   创 建 者：20182330-wby
*   创建日期：2019年09月24日
*   描    述：
*
================================================================*/
public class Die{
    private final int MAX = 6;
    private int faceValue;
    
    public Die()
    {
        faceValue = 1;
    }

    public int roll()
    {
        faceValue = (int)(Math.random()*MAX)+1;
        return faceValue;
    }

    public void setFaceValue(int value)
    {
        if(value>0&&value<=MAX)
            faceValue = value;
    }

    public int getFaceValue()
    {
        return faceValue;
    }

    public String toString()
    {
        String result = Integer.toString(faceValue);

        return result;
    }
}

