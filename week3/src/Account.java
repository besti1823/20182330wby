/*================================================================
*   Copyright (C) 2019 20182330-wby All rights reserved.
*   
*   文件名称：Account.java
*   创 建 者：20182330-wby
*   创建日期：2019年09月24日
*   描    述：
*
================================================================*/
import java.text.NumberFormat;
public class Account{
    public static void main(Strings[] args){
        private final double RATE = 0.035;

        private String name;
        private long acctNumber;
        private double balance;

        public Account (String owner,long account,double initial)
        {
            name = owner;
            acctNumber = account;
            balance = initial;
        }
        public double deposit (double amount)
        {
            if(amount>0)
                balance = balance + amount;
            return balance;
        }
        public double withdraw(double amount,double fee)
        {
            if(amount+fee>0&& amount+fee<balance)
                balance = balance - amount - fee;

            return balance;
        }

        public double addInterest()
        {
            balance +=(balance * RATE);
            return balance;
        }

        public double getBalance()
        {
            return balance;
        }

        public String toString()
        {
         NumberFormat fmt = MumberFormat.getCurrencyInstance();

     return (acctnumber +"\t"+name+"\t"+fmt.format(balance));
        }
    }



