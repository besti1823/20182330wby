public class Book {
    
      String bookname, author, press, copyrightdate;
        
      public Book(String b, String a, String p, String c) {
                    bookname = b;
                    author = a;
                    press = p;
                    copyrightdate = c;
                   }
            
      public void setBookname(String bookname1) {
                  bookname = bookname1;
                                }
                
      public String getBookname() {
                  return bookname;
                              }
                    
     public void setAuthor(String author1) {
                   author = author1;
                            }
                        
      public String getAuthor() {
                   return author;
                             }
                            
      public void setPress(String press1) {
                       
                   press = press1;
                               }
                                
      public String getPress() {
                    return press;
                               }
                                    
      public void setCopyrightdate(String copyrightdate1) {
                     copyrightdate = copyrightdate1;
                                }
                          
      public String getCopyrightdate() {
                    return copyrightdate;
                                 }
                                           
      public String toString() {
                    return bookname + author + press + copyrightdate;
                                  }
 
      public static void main(String[] args)
        {
               Book book1 = new Book("yuwen", "PEP", "beijing", "2019.1");
               System.out.println(book1);

               Book book2 = new Book("yingyu", "PEP", "aaa", "2019.11");
               System.out.println(book2);

               book1.setBookname("yuwen");
               book1.setAuthor("PEP");
               book1.setPress("beijing");
               book1.setCopyrightdate("2019.1");
               System.out.println(book1.bookname);
          }

}
