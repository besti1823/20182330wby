
/*================================================================
*   Copyright (C) 2019 20182330-wby All rights reserved.
*   
*   文件名称：bookshelf.java
*   创 建 者：20182330-wby
*   创建日期：2019年09月20日
*   描    述：
*
================================================================*/
package com.company.k;


public class Bookshelf {
     public static void main(String[] args)
        {
              Book book1 = new Book("yuwen", "PEP", "beijing", "2019.1");
              System.out.println(book1);

              Book book2 = new Book("yingyu", "PEP", "aaa", "2019.11");
              System.out.println(book2);

              book1.setBookname("yuwen");
              book1.setAuthor("PEP");
              book1.setPress("beijing");
              book1.setCopyrightdate("2019.1");
              System.out.println(book1.bookname);
         }
}


