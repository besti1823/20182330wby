/*================================================================
*   Copyright (C) 2019 20182330-wby All rights reserved.
*   
*   文件名称：Coin.java
*   创 建 者：20182330-wby
*   创建日期：2019年09月24日
*   描    述：
*
================================================================*/
public class Coin{
    public static void main(String[] args){
        private final int HEADS = 0;
        private int face;
        
        public Coin ()
        {
            flip();
        }

        public void flip()
        {
            face = (int)(Math.random()*2);
        }
        public boolean isHeads()
        {
            return (face == HEADS);
        }
        public String toString()
        {
            return (face == HEADS)?"Heads":"Tails";
        }
    }

