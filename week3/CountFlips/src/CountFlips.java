/*================================================================
*   Copyright (C) 2019 20182330-wby All rights reserved.
*   
*   文件名称：CountFlips.java
*   创 建 者：20182330-wby
*   创建日期：2019年09月24日
*   描    述：
*
================================================================*/
public class CountFlips{
    public static void main(String[] args){
        final int FLIPS = 1000;
        int heads = 0,tails = 0;

        Coin myCoin = new Coin();

        for (int count=1;count<=FLIPS;count++)
        {
            myCoin.flip();
            if (myCoin.isHeads())
                heads++;
            else
                tails++;
        }
        System.out.println("Number of flips: "+ FLIPS);
        System.out.println("Number of heads: "+ heads);
        System.out.println("Number of tails: "+ tails);
    }
}

