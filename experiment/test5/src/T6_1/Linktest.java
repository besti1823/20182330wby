package T6_1;

import java.util.Scanner;

public class Linktest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please input your studentnumber and the time (eg: 20，18，23，30，14，4，30)");
        String sen = scan.nextLine();
        int[] number = new int[100];//定义数组
        String[] numbers = sen.split("，");//用，分割字符串

        //将字符串转化为int型并且放入数组
        for (int m = 0; m < numbers.length; m++) {
            number[m] = Integer.parseInt(numbers[m]);
        }

        //实例化并且定义Head的表头
        Link Head = new Link(number[0]);

        //形成数组链表
        int nWeiBingYan = 1;
        for (int a = 1; a < numbers.length; a++) {
            Link node = new Link(number[a]);
            InsertNode(Head, node);
            nWeiBingYan++;
        }

        //打印链表
        System.out.println("The linklist is ：");
        PrintLink(Head);
        System.out.println();
        System.out.println("There are " + nWeiBingYan + " numbers in the list");

    }

    //输出链表
    public static void PrintLink(Link Head) {
        Link node = Head;
        while (node != null) {
            System.out.print(node.number+" ");
            node = node.next;
        }
    }

    //尾插法
    public static void InsertNode(Link Head, Link node) {
        Link temp = Head;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = node;
    }

}