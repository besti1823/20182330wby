package com.example.toast;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Toast toast = Toast.makeText(MainActivity.this, "20182330魏冰妍!", Toast.LENGTH_LONG); toast.show();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}