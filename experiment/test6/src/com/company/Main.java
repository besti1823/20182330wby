package com.company;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

import static com.company.Link.*;


public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please input your studentnumber and the time (eg: 20，18，23，30，14，4，30)");
        String sen = scan.nextLine();
        int wby = 0, number2;
        int[] number = new int[100];//定义数组
        String[] numbers = sen.split(",");//用，分割字符串

        //将字符串转化为int型并且放入数组
        for (int m = 0; m < numbers.length; m++) {
            number[m] = Integer.parseInt(numbers[m]);
        }

        //实例化并且定义Head的表头
        Link Head = new Link(number[0]);

        //形成数组链表
        int nWeiBingYan = 1;
        for (int a = 1; a < numbers.length; a++) {
            Link node = new Link(number[a]);
            Head.BackLink(Head, node);
            nWeiBingYan++;
        }

        //创建文件
        File file = new File("LinkNum.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        Writer writer1 = new FileWriter(file);
        writer1.write("2018 ");
        writer1.flush();


        writer1.append("2330");
        writer1.flush();


        Reader reader = new FileReader(file);
        BufferedReader bufferedreader = new BufferedReader(reader);
        StringTokenizer stringTokenizer = new StringTokenizer(bufferedreader.readLine());
        number2 = stringTokenizer.countTokens();
        int[] ints = new int[number2];
        while (stringTokenizer.hasMoreTokens()) {
            ints[wby] = Integer.parseInt(stringTokenizer.nextToken());
            wby++;
        }

        Link q = new Link(ints[0]);
        Link p = new Link(ints[1]);

        System.out.println("The list is: ");
        Head.toString();
        System.out.println();
        System.out.println("There are " + nWeiBingYan + " numbers in the list");

        Link Head1=HeadInsert(Head, p);
        System.out.println("There are " + nWeiBingYan++ + " numbers in the list");
        System.out.println();


        MidInsert(Head1, q, 5);
        System.out.println("There are " + nWeiBingYan++ + " numbers in the list");
        System.out.println();

        DeleteLink(Head1, 5);
        System.out.println("There are " + nWeiBingYan-- + " numbers in the list");
        System.out.println();

        SelectionSort(Head1);
        System.out.println("There are " + nWeiBingYan + " numbers in the list");
        System.out.println();
    }






}

}
