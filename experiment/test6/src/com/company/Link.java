package com.company;

public class Link {
    protected int number;
    protected Link next = null;

    public Link(int number){
        this.number = number;
    }

    public String toString()
    {
        Link node =this;
        while (node!=null) {
            System.out.print(node.number+" ");
            node = node.next;
        }
        System.out.println();
        return null;
    }

    //尾插法
    public void BackLink(Link Head,Link node1){
        Link temp=Head;
        while(temp.next!=null)
            temp=temp.next;

        temp.next=node1;
    }

    //头插法
    public static Link HeadInsert(Link Head,Link node){
        System.out.println("The list headinserted is: ");
        node.next=Head;
        Head=node;
        Head.toString();
        return Head;
    }

    //中间插入
    public  static void MidInsert(Link Head,Link node1,int place){
        System.out.println("The list midinserted is: ");
        Link temp= Head;

        int a=1;
        while(a<4){
            temp= temp.next;
            a++;
        }
        node1.next=temp.next;
        temp.next=node1;
        Head.toString();

    }

    //删除
    public static void DeleteLink(Link Head, int t) {//删除可以指向他的下一个元素
        System.out.println("The list deleted is: ");
        Link temp = Head;
        Link temp1=Head;
        Link node=Head;
        for (int i = 0; i < t-2; i++) {
            temp1 = temp1.next;
        }
        temp1.next = temp1.next.next;
        while (temp1!=null) {
            temp1 = temp1.next;
        }
        Head.toString();
    }

    //选择排序
    public static void SelectionSort(Link Head) {
        System.out.println("The list after selectiondort is: ");
        int temp;
        Link node = Head;
        while (node != null) {
            Link reNode = node.next;
            while (reNode != null) {
                if (reNode.number > node.number) {
                    temp = reNode.number;
                    reNode.number = node.number;
                    node.number = temp;
                }
                reNode = reNode.next;
            }
            node = node.next;
        }
        Head.toString();
    }




}