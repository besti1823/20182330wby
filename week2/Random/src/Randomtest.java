/*================================================================
*   Copyright (C) 2019 20182330-wby All rights reserved.
*   
*   文件名称：Random.java
*   创 建 者：20182330-wby
*   创建日期：2019年09月18日
*   描    述：Random and NumberFormat
*
================================================================*/
import java.util.*;
import java.text.*;
public class Randomtest
{
    public static void main(String[] args)
    {
        Random generator = new Random();
        float a;
        a = generator.nextFloat()*21-10;
        DecimalFormat fmt = new DecimalFormat("0.###");
        System.out.println("From -10 to 10:" + a);
        System.out.println("From -10 to 10:" + fmt.format(a));
    }
}

