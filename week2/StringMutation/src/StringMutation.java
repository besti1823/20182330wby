/*================================================================
*   Copyright (C) 2019 20182330-wby All rights reserved.
*   
*   文件名称：StringMutation.java
*   创 建 者：20182330-wby
*   创建日期：2019年09月24日
*   描    述：
*
================================================================*/
public class StringMutation {
    public static void main(String[] args){
        String phrase = "Change is inevitable";
        String mutation1,mutation2,mutation3,mutation4;
        System.out.println("Original string :\""+phrase +"\"");
        System.out.println("Length of string: "+phrase.length());

        mutation1 = phrase.concat (",except from vending"
                +" machines.");
                          
        mutation2 = mutation1.toUpperCase();
        mutation3 = mutation2.replace('E','x');
        mutation4 = mutation3.substring(3,30) ;
        System.out.println("Mutation #1: "+mutation1);
        System.out.println("Mutation #2: "+mutation2);
        System.out.println("Mutation #3: "+mutation3);
        System.out.println("Mutation #4: "+mutation4);
    }
}


