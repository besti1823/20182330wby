package pp13_2;

public class content implements Comparable
{
    private String firstN, lastN, phone;
    public content(String first, String last, String telephone)
    {
        firstN = first;
        lastN = last;
        phone = telephone;
    }
    public String toString ()
    {
        return lastN + ", " + firstN + ":  " + phone;
    }
    public int compareTo (Object other)
    {
        int result;
        result = phone.compareTo(((content)other).phone);
        return result;
    }

    public static void select(content[] a){
        content t;
        int mix;
        for(int i=0;i<a.length-1;i++)
        {
            mix=i;
            for(int j=i+1;j<a.length;j++)
                if(a[j].phone.compareTo(a[mix].phone)>0)
                    mix=j;

            if(i!=mix)
            {
                t=a[i];
                a[i]=a[mix];
                a[mix]=t;
            }
        }
        for(int m=0;m<a.length;m++)
            System.out.println(a[m]);
    }
}