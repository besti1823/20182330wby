package com.example.myapplication;

import java.util.Scanner;
import java.util.Stack;
public class Postfixvaluar {
    private final char Add='+';
    private final char Sub='-';
    private final char Mul='*';
    private final char Div='/';
    private Stack<Integer> stack;

    public Postfixvaluar()
    {
        stack = new Stack<Integer>();
    }
    public int evaluate(String s) {
        int op1,op2,m=0;
        String token ;
        Scanner tokenizer = new Scanner(s);

        while (tokenizer.hasNext())
        {
            token = tokenizer.next();
            if(isOpearator(token))//碰见符号弹栈
            {
                op2 = (stack.pop()).intValue();
                op1 = (stack.pop()).intValue();
                m = evalSingleop(token.charAt(0),op1,op2);
                stack.push(m);
            }
            else
                stack.push(Integer.parseInt(token));//否则压栈
        }

        return m;
    }

    public boolean isOpearator(String token) {
        return (token.equals("+")||token.equals("-")||token.equals("/")||token.equals("*"));
    }

    public int evalSingleop(char op,int op1,int op2) {
        int answer = 0;
        switch (op)
        {
            case Add:
                answer=op1+op2;
                break;
            case Sub:
                answer = op1-op2;
                break;
            case Mul:
                answer = op1*op2;
                break;
            case Div:
                answer= op1/op2;

        }

        return answer;

    }
}