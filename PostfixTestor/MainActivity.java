package com.example.myapplication;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    AutoCompleteTextView vi;
    AutoCompleteTextView vi2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vi = findViewById(R.id.autoCompleteTextView2);
        vi2 = findViewById(R.id.autoCompleteTextView3);


        Button cal = findViewById(R.id.button2);
        cal.setOnClickListener(onClickListener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String s1 = vi.getText().toString();
             Postfixvaluar number = new Postfixvaluar();
             int result = number.evaluate(s1);
            Intent intent = new Intent(MainActivity.this,Second.class);
            intent.putExtra("message", result);
            vi2.setText(result+"");
        }
    };
}
