
public class Leaf {
    protected int data;
    public Leaf low;
    public Leaf high;
    public Leaf(int data){
        this.data=data;
        low=null;
        high=null;
    }
}