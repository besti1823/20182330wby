package com.company;

import java.util.Iterator;

public class CircularArrayQueue<T> implements Queue<T> {
    private final int DEFAULT_CAPACITY = 10;
    private int front, rear, count;
    private T[] queue;

    public CircularArrayQueue() {
        front = rear = count = 0;
        queue = (T[]) (new Object[DEFAULT_CAPACITY]);

    }

    public void enqueue(T element){
        if(count == queue.length)
            expandCapacity();
        queue[rear]=element;
        rear=(rear+1)%queue.length;
        count++;
    }

    private void expandCapacity() {
        T[] larger = (T[]) (new Object[queue.length * 2]);
        for (int index = 0; index < count; index++)
            larger[index] = queue[(front+index)%queue.length];

        front = 0;
        rear = count;
        queue = larger;
    }

    public T dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = queue[front];
        queue[rear] = null;
        front = (front + 1) % queue.length;

        count--;

        return result;
    }

    public T first() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        return queue[front];
    }

    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        return count;
    }

    public String toString()//输出String型duilie中数据
    {
        String last = "";
        int temp = front;
        for (int a = 0; a < size(); a++) {
            last += queue[temp] + " ";
            temp = (temp + 1) % queue.length;
        }
        return last;
    }

}
